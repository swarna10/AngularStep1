import { Component, OnInit } from '@angular/core';
import { Circle , CIRCLE } from './circle' ;

@Component({
  selector: 'app-circle',
  templateUrl: './circle.component.html',
  styleUrls: ['./circle.component.css']
})
export class CircleComponent implements OnInit {

  constructor() { }
  circles : Circle [] ;

  ngOnInit() {
    this.circles = CIRCLE ;
  }

}
